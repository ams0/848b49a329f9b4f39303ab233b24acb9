#!/bin/bash

ARCH=$(dpkg --print-architecture)

wget -q "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/${ARCH}/kubectl"
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
rm kubectl

# Enable bash completion for kubectl
echo "source /usr/share/bash-completion/bash_completion" >> /root/.bashrc
echo "complete -F __start_kubectl k" >> /root/.bashrc

# Upgrade KinD
wget -q https://github.com/kubernetes-sigs/kind/releases/download/v0.17.0/kind-linux-${ARCH} -O kind
chmod +x ./kind
sudo mv ./kind /usr/local/bin/kind

#Helm
HELM_RELEASE=$(curl -L -s -H 'Accept: application/json' https://github.com/helm/helm/releases/latest| sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')
tar -xvz -C /tmp/ -f <(wget -q -O - https://get.helm.sh/helm-${HELM_RELEASE}-linux-${ARCH}.tar.gz)
install -o root -g root -m 0755 /tmp/linux-${ARCH}/helm /usr/local/bin/helm

wget -q https://go.dev/dl/go1.20.2.linux-${ARCH}.tar.gz
tar -xvf go1.20.2.linux-${ARCH}.tar.gz
mv go /usr/local
echo "export GOROOT=/usr/local/go" >> /etc/bash.bashrc
echo "export PATH=$GOPATH/bin:$GOROOT/bin:$PATH" >> /etc/bash.bashrc
ln -sf /usr/local/go/bin/go /bin/go

apt-get install -y -qq make g++ jq
curl -fsSL get.docker.com -o get-docker.sh && sudo sh get-docker.sh && sudo systemctl start docker && sudo systemctl enable docker
sudo usermod -aG docker ubuntu